import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Content } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-doc',
  templateUrl: 'doc.html',
})
export class DocPage {

  @ViewChild(Content) content:Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DocPage');
  }

  modalParametros(){
    const modal = this.modalCtrl.create('ModalIndicePage', null, {
      cssClass: 'modal'
    });
    modal.onDidDismiss(data=> {
      this.scrollTo(data);
    })
    modal.present();


  }

  scrollTo(id){
    if (id!=null){
      let height = document.getElementById(id).offsetTop;
      this.content.scrollTo(0,height+60, 700);
    }
  }

}

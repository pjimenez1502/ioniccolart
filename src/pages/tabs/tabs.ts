import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root:string = 'HomePage';
  tab2Root:string = 'GuiaPage';
  tab3Root:string = 'DocPage';

  constructor() {

  }
}

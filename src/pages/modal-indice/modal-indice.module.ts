import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalIndicePage } from './modal-indice';

@NgModule({
  declarations: [
    ModalIndicePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalIndicePage),
  ],
})
export class ModalIndicePageModule {}

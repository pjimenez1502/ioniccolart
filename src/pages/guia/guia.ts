import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-guia',
  templateUrl: 'guia.html'
})
export class GuiaPage {

  @ViewChild(Content) content:Content;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GuiaPage');
  }

  scrollTo(id){
    let height = document.getElementById(id).offsetTop;
    this.content.scrollTo(0,height, 700);
  }

}

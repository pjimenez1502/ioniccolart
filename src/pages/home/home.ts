import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  disableAnBtt = false;
  disableIOsBtt = false;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public platform: Platform,
              public iab:InAppBrowser,
              public toastCtrl:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');

    if(this.platform.is('android'))
    this.disableIOsBtt= true;

    if(this.platform.is('ios'))
    this.disableAnBtt = true;
  
  }

  openLink(url:string){
    this.iab.create(url, '_system');
  }

  openToast(){
    const toast = this.toastCtrl.create({
      message: 'iOs App en construcción',
      duration: 1500
    });
    toast.present();
  }

}

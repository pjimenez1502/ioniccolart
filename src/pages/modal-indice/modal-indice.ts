import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalIndicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-indice',
  templateUrl: 'modal-indice.html',
})
export class ModalIndicePage {

  sections = [
      {name:"1. Descripción de la aplicación", ref:"sec1"},
      {name:"2. Público objetivo", ref:"sec2"},
      {name:"3. Funcionalidades de la aplicación", ref:"sec3"},
      {name:"4. Interacciones", ref:"sec4"}
    ]

  sectsShow = this.sections;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalIndicePage');
  }

  closeModal(id){
    this.viewCtrl.dismiss(id);
  }

  search(ev){
    let ser = ev.target.value;  

    if(ser && ser.trim() != ''){
      this.sectsShow = this.sectsShow.filter((sec) => {
        return (sec.name.toLowerCase().indexOf(ser.toLowerCase()) > -1)
      })
    }else {
      this.sectsShow = this.sections;
    }
  }

}
